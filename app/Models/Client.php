<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Client extends Authenticatable
{
    use HasFactory, HasApiTokens;
    protected $table = 'clients';
    protected $fillable = [
        'address_id',
        'type',
        'username',
        'password',
        'full_name',
        'age',
        'gender',
        'email',
        'phone',
        'token_mail',
        'status',
        'ip_last_login',
        'token_device',
    ];
    protected $primaryKey = "id";

    public function address(){
        return $this->hasOne(Address::class);
    }
    public function students(){
        return $this->hasMany(Student::class);
    }
    public function parent(){
        return $this->belongsTo(Student::class);
    }
}
