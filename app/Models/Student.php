<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    use HasFactory;
    protected $table = 'students';
    protected $fillable = [
        'student_id',
        'parent_id',
    ];
    protected $primaryKey = "id";

    public function client(){
        return $this->belongsTo(Client::class);
    }
    public function parent(){
        return $this->hasOne(Client::class);
    }

}
