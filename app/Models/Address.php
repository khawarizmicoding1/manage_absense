<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    use HasFactory;
    protected $table = 'addresses';
    protected $fillable = [
        'country',
        'city',
        'state',
        'zip_code',
        'line_first',
        'line_second',
    ];
    protected $primaryKey = "id";

    public function admin(){
        return $this->belongsTo(Admin::class);
    }
    public function client(){
        return $this->hasOne(Client::class);
    }
}
