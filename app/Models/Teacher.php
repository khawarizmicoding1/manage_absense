<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
    use HasFactory;
    protected $table = 'teachers';
    protected $fillable = [
        'admin_id',
        'grade',
    ];
    protected $primaryKey = "id";

    public function admin(){
        $this->belongsTo(Admin::class);
    }
}
