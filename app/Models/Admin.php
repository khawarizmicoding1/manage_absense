<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Admin extends Model
{
    use HasFactory;

    protected $table = 'admins';
    protected $fillable = [
        'address_id',
        'role',
        'username',
        'password',
        'full_name',
        'age',
        'gender',
        'email',
        'phone',
        'token_mail',
        'status',
        'ip_last_login',
        'token_device',
    ];
    protected $primaryKey = "id";

    public function address(){
        return $this->hasOne(Address::class);
    }

    public function teachers(){
        return $this->hasMany(Teacher::class);
    }

}
