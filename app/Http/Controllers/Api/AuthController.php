<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateClientRequest;
use App\services\AuthService;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function singUp(CreateClientRequest $request){
        $service = new AuthService($request);
        return response()->json($service->singUp());
    }

    public function login(Request $request){
        $request->validate([
            'email' => 'required|email',
            'password' => 'required',
        ]);
        $service = new AuthService($request);
        return response()->json($service->login());

    }

    public function checkAccountAndGenerateToken(Request $request){
        $request->validate([
            'email' => 'required|email'
        ]);
        $service = new AuthService($request);
        return response()->json($service->checkAccountAndGenerateToken());
    }

    public function checkTokenMail(Request $request){
        $request->validate([
            'token_mail' => 'required'
        ]);
        $service = new AuthService($request);
        return response()->json($service->checkTokenMail());
    }

    public function getInfo(Request $request){
        $service = new AuthService($request);
        return response()->json($service->getInfo());
    }

    public function updateInfo(Request $request){
        $request->validate([
            'full_name' => 'required',
            'age' => 'required|max:12',
            'gender' => 'required|in:women,men',
        ]);
        $service = new AuthService($request);
        return response()->json($service->updateInfo());
    }
}
