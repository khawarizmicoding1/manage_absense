<?php

namespace App\Http\Controllers;

use App\Http\Requests\AdminRequest;
use App\Http\Resources\AdminResource;
use App\Models\Address;
use App\Models\Admin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller
{
    public function store(AdminRequest $request){

        $address = Address::query()->create([
            'country' => $request->input('country'),
            'city' => $request->input('city'),
            'state' => $request->input('state'),
            'zip_code' => $request->input('zip_code'),
            'line_first' => $request->input('line_first'),
            'line_second' => $request->input('line_second'),
        ]);

        $admin = Admin::query()->create([
            'address_id' => $address->getAttribute('id'),
            'username' => $request->input('username'),
            'password' => Hash::make($request->input('password')),
            'age' => $request->input('age'),
            'gender' => $request->input('gender'),
            'email' => $request->input('email'),
            'phone' => $request->input('phone'),
        ]);

        return new AdminResource(Admin::with('address')->find($admin->getAttribute('id')));

    }
}
