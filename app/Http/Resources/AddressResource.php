<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class AddressResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'country' => $this->id,
            'city' => $this->id,
            'zip_code' => $this->id,
            'line_first' => $this->line_first,
            'line_second' => $this->line_second,
        ];
    }
}
