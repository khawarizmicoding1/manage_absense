<?php

namespace App\services;

use App\Http\Resources\ClientResource;
use App\Models\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class AuthService
{
    private $request;
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function singUp(){
        try {
            $data = $this->request->toArray();
            $data['password'] = Hash::make($data['password']);
            $client = Client::query()->create($data);
            return new ClientResource($client);
        }catch (\Exception $e){
            Log::error('[AuthService] singUp exception::', [
                'data' => $e
            ]);
            return [
                'status' => false,
                'message' => 'Server error',
            ];
        }
    }
    public function login(){
        $client = Client::query()
            ->where('email', $this->request->input('email'))->first();

        if($client == null){
            Log::warning('[AuthService] login client not found::', [
                'email' => $this->request->input('email')
            ]);
            return [
                "status" => false,
                "message" => "Account invalid",
            ];
        }
        if(!password_verify($this->request->input('password'), $client->getAttribute('password'))){
            Log::warning('[AuthService] login client not found::', [
                'email_exist' => $this->request->input('email'),
                'password' => $this->request->input('password')
            ]);
            return [
                "status" => false,
                "message" => "Account invalid",
            ];
        }

        $token = $client->createToken('token')->plainTextToken;

        return [
            'status'=> true,
            'token' => $token
        ];

    }

    public function checkAccountAndGenerateToken(){
        $client = Client::query()
            ->where('email', $this->request->input('email'))->first();

        if($client == null){
            Log::warning('[AuthService] checkAccount client not found::', [
                'email' => $this->request->input('email')
            ]);
            return [
                "status" => false,
                "message" => "Account invalid",
            ];
        }
        $tokenMail = md5($client->id.time());
        $client->update([
            'token_mail' => $tokenMail,
        ]);
        //
        return [
            "status" => true,
            "token_mail" => $tokenMail,
        ];
    }

    public function checkTokenMail(){
        $client = Client::query()
            ->where('token_mail', $this->request->input('token_mail'))->first();

        if($client == null){
            Log::warning('[AuthService] checkTokenMail client not found::', [
                'token_mail' => $this->request->input('token_mail')
            ]);
            return [
                "status" => false,
                "message" => "Account invalid",
            ];
        }

        $client->update([
            'password' => Hash::make($this->request->input('password'))
        ]);

        return [
            "status" => true,
            "message" => "Password changed",
        ];

    }

    public function getInfo(){
        $client = Client::query()->find(Auth::id());
        return new ClientResource($client);
    }

    public function updateInfo(){
        $client = Client::query()->find(Auth::id());
        if($client == null){
            Log::warning('[AuthService] updateInfo client not found');
            return [
                "status" => false,
                "message" => "Account invalid",
            ];
        }
        $client->update($this->request->toArray());

        return new ClientResource($client);
    }



}
