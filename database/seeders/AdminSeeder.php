<?php

namespace Database\Seeders;

use App\Models\Admin;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Admin::query()->create([
            "username" => 'admin',
            "password" => Hash::make('admin@123'),
            "full_name" => 'Admin test',
            "email" => 'admin@admin.com',
            "phone" => '0600000000',
        ]);
    }
}
