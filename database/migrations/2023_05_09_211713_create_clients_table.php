<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('address_id')->nullable();
            $table->foreign('address_id')->references('id')->on('addresses');

            $table->enum('type', ['student', 'parent'])->nullable();
            $table->string('username', 12);
            $table->string('password')->nullable();
            $table->string('full_name')->nullable();
            $table->integer('age')->nullable();
            $table->enum('gender', ['women', 'men'])->nullable();
            $table->string('email')->unique()->nullable();
            $table->string('phone')->unique()->nullable();
            $table->string('token_mail')->nullable();
            $table->boolean('status')->default(false);
            $table->string('ip_last_login')->nullable();
            $table->text('token_device')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('clients');
    }
};
