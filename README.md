# Manage absence


## Getting started

```
git clone https://gitlab.com/khawarizmicoding1/manage_absense.git
cd manage_absense
composer install
npm install
php artisan key:generate
```

## Setup 01:
### Creating & configuration all table auth
```
php artisan make:model Address -m
php artisan make:model Admin -m
php artisan make:model Client -m
php artisan make:model Student -m
php artisan make:model Teacher -m
```
### Creating seeder & run migration with seeder
```
php artisan make:seeder AdminSeeder
php artisan migrate:fresh --seed
```

