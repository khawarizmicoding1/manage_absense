<?php

use App\Http\Controllers\Api\AuthController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->group(function (){
    Route::get('info', [AuthController::class, 'getInfo']);
    Route::post('info/update', [AuthController::class, 'updateInfo']);
});

Route::post('register', [AuthController::class, 'singUp']);
Route::post('login', [AuthController::class, 'login']);
Route::post('check', [AuthController::class, 'checkAccountAndGenerateToken']);
Route::post('forget', [AuthController::class, 'checkTokenMail']);

